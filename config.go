/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package nsqclient

import btls "test.io/third/nsqclient/tls"

type NSQConfig struct {
	Address     string      `json:"nsqd_tcp_address"`
	Topic       string      `json:"topic"`
	UserAgent   string      `json:"user_agent"`
	TLS         btls.Config `json:"tls"`
	MaxInFlight int         `json:"max_in_flight"`
}

// NewNSQConfig creates a new NSQConfig with default values.
func NewNSQConfig() NSQConfig {
	return NSQConfig{
		Address:     "",
		Topic:       "",
		UserAgent:   "",
		TLS:         btls.NewConfig(),
		MaxInFlight: 64,
	}
}
