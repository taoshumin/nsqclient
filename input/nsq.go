/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package input

import (
	"crypto/tls"
	"strings"
	"sync"

	"github.com/nsqio/go-nsq"
)

// NSQ is an input type that receives NSQ messages.
type NSQ struct {
	consumer *nsq.Consumer
	cMut     sync.Mutex

	tlsConf         *tls.Config
	addresses       []string
	lookupAddresses []string
	conf            NSQConfig

	internalMessages chan *nsq.Message
	interruptChan    chan struct{}
}

func NewNSQ(conf NSQConfig) (*NSQ, error) {
	n := NSQ{
		conf:             conf,
		internalMessages: make(chan *nsq.Message),
		interruptChan:    make(chan struct{}),
	}

	for _, addr := range conf.Addresses {
		for _, splitAddr := range strings.Split(addr, ",") {
			if len(splitAddr) > 0 {
				n.addresses = append(n.addresses, splitAddr)
			}
		}
	}

	for _, addr := range conf.LookupAddresses {
		for _, splitAddr := range strings.Split(addr, ",") {
			if len(splitAddr) > 0 {
				n.lookupAddresses = append(n.lookupAddresses, splitAddr)
			}
		}
	}

	if conf.TLS.Enabled {
		var err error
		if n.tlsConf, err = conf.TLS.Get(); err != nil {
			return nil, err
		}
	}
	return &n, nil
}

func (n *NSQ) Connect() (err error) {
	n.cMut.Lock()
	defer n.cMut.Unlock()

	if n.consumer != nil {
		return nil
	}

	cfg := nsq.NewConfig()
	cfg.UserAgent = n.conf.UserAgent
	cfg.MaxInFlight = n.conf.MaxInFlight
	if n.tlsConf != nil {
		cfg.TlsV1 = true
		cfg.TlsConfig = n.tlsConf
	}

	var consumer *nsq.Consumer
	if consumer, err = nsq.NewConsumer(n.conf.Topic, n.conf.Channel, cfg); err != nil {
		return
	}

	consumer.AddHandler(n)

	if err = consumer.ConnectToNSQDs(n.addresses); err != nil {
		consumer.Stop()
		return
	}

	if err = consumer.ConnectToNSQLookupds(n.lookupAddresses); err != nil {
		consumer.Stop()
		return
	}
	n.consumer = consumer
	return
}

//func (n *NSQ) read(ctx context.Context) (*nsq.Message, error) {
//	var msg *nsq.Message
//	select {
//	case msg = <-n.internalMessages:
//		return msg, nil
//	case <-ctx.Done():
//	case <-n.interruptChan:
//		for _, m := range n.unAckMsgs {
//			m.Requeue(-1)
//			m.Finish()
//		}
//		n.unAckMsgs = nil
//		_ = n.disconnect()
//		return nil, component.ErrTypeClosed
//	}
//	return nil, component.ErrTimeout
//}

//func (n *NSQ) ReadWithContext(ctx context.Context) (*message.Batch, AsyncAckFn, error) {
//	msg, err := n.read(ctx)
//	if err != nil {
//		return nil, nil, err
//	}
//	n.unAckMsgs = append(n.unAckMsgs, msg)
//	return message.QuickBatch([][]byte{msg.Body}), func(rctx context.Context, res error) error {
//		if res != nil {
//			msg.Requeue(-1)
//		}
//		msg.Finish()  // 消息发送成功以后Finish
//		return nil
//	}, nil
//}

func (n *NSQ) HandleMessage(message *nsq.Message) error {
	message.DisableAutoResponse()
	select {
	case n.internalMessages <- message:
	case <-n.interruptChan:
		message.Requeue(-1)
		message.Finish()
	}
	return nil
}

func (n *NSQ) Close() error {
	n.cMut.Lock()
	defer n.cMut.Unlock()

	if n.consumer != nil {
		n.consumer.Stop()
		n.consumer = nil
	}
	return nil
}
